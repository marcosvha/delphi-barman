unit BarManFunctions;

interface
uses ZDataset, ZConnection, Variants;

function GetNextKey(TableName, KeyField: string; Conn: TZConnection): integer;
function HasChildRegisters(TableName, KeyField, KeyValueInt: string; Conn: TZConnection): boolean;
function PlayQuery(SQL: string; Conn: TZConnection): string;
procedure ExecQuery(SQL: string; Conn: TZConnection);


implementation

uses DB;

{-----------------------------------------------------------------------------
  Procedure: GetNextKey
  Author:    Alemao
  Date:      07-ago-2004
  Arguments: TableName, KeyField: string; Connection: TZConnection
  Result:    Retorna o pr�ximo n�mero do campo e tabela indicados
-----------------------------------------------------------------------------}
function GetNextKey(TableName, KeyField: string; Conn: TZConnection): integer;
var
  qryAux: TZQuery;
begin
  Result:= 1;
  try
    qryAux:= TZQuery.Create(nil);
    try
      qryAux.Connection:= Conn;
      qryAux.SQL.Text:= 'select max('+ KeyField + ') from ' + TableName;
      qryAux.Open;
      Result:= qryAux.Fields[0].AsInteger +1;
      qryAux.Close;
    finally
      qryAux.Free;
    end;
  except
  end;
end;

function HasChildRegisters(TableName, KeyField, KeyValueInt: string; Conn: TZConnection): boolean;
var
  qryAux: TZQuery;
begin
  Result:= false;
  try
    qryAux:= TZQuery.Create(nil);
    try
      qryAux.Connection:= Conn;
      qryAux.SQL.Text:= 'select '+ KeyField + ' from ' + TableName +
                        'where ' + KeyField + '=' + KeyValueInt;
      qryAux.Open;
      Result:= not (qryAux.Eof and qryAux.Bof);
      qryAux.Close;
    finally
      qryAux.Free;
    end;
  except
  end;
end;

function PlayQuery(SQL: string; Conn: TZConnection): string;
var
  qryAux: TZQuery;
begin
  Result:= '';
  try
    qryAux:= TZQuery.Create(nil);
    try
      qryAux.Connection:= Conn;
      qryAux.SQL.Text:= SQL;
      qryAux.Open;
      Result:= qryAux.Fields[0].AsString;
      qryAux.Close;
    finally
      qryAux.Free;
    end;
  except
  end;
end;

procedure ExecQuery(SQL: string; Conn: TZConnection);
var
  qryAux: TZQuery;
begin
  try
    qryAux:= TZQuery.Create(nil);
    try
      qryAux.Connection:= Conn;
      qryAux.SQL.Text:= SQL;
      qryAux.ExecSQL;
      qryAux.Close;
    finally
      qryAux.Free;
    end;
  except
  end;
end;

end.
