object frmCategoryCad: TfrmCategoryCad
  Left = 308
  Top = 247
  Width = 402
  Height = 423
  Caption = 'Cadastro de categorias'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    394
    389)
  PixelsPerInch = 96
  TextHeight = 13
  object grdInvoiceItems: TJvDBGrid
    Left = 0
    Top = 0
    Width = 394
    Height = 345
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsNone
    Color = 15987699
    DataSource = dtsInvoiceItem
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    ParentFont = False
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    MultiSelect = True
    TitleButtons = True
    Columns = <
      item
        Color = clSilver
        Expanded = False
        FieldName = 'categorycod'
        ReadOnly = True
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'categoryname'
        Title.Caption = 'Nome da categoria'
        Width = 300
        Visible = True
      end>
  end
  object DBNavigator: TDBNavigator
    Left = 8
    Top = 354
    Width = 228
    Height = 25
    DataSource = dtsInvoiceItem
    VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel, nbRefresh]
    Anchors = [akLeft, akBottom]
    Hints.Strings = (
      'First record'
      'Prior record'
      'Next record'
      'Last record'
      'Incluir'
      'Excluir'
      'Alterar'
      'Gravar altera'#231#245'es'
      'Cancelar altera'#231#245'es'
      'Atualizar')
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    BeforeAction = DBNavigatorBeforeAction
  end
  object qrycategory: TZQuery
    Connection = frmMain.conBarMan
    RequestLive = True
    CachedUpdates = False
    BeforePost = qrycategoryBeforePost
    SQL.Strings = (
      'select * from category'
      'order by categoryname')
    ParamCheck = True
    Params = <>
    ShowRecordTypes = [utModified, utInserted, utUnmodified]
    Top = 136
  end
  object dtsInvoiceItem: TDataSource
    DataSet = qrycategory
    Left = 32
    Top = 136
  end
end
