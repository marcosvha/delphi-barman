unit CategoryCad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ZAbstractRODataset, ZAbstractDataset, ZDataset, Grids,
  DBGrids, JvDBCtrl, ExtCtrls, DBCtrls;

type
  TfrmCategoryCad = class(TForm)
    grdInvoiceItems: TJvDBGrid;
    qrycategory: TZQuery;
    dtsInvoiceItem: TDataSource;
    DBNavigator: TDBNavigator;
    procedure qrycategoryBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBNavigatorBeforeAction(Sender: TObject;
      Button: TNavigateBtn);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure OpenGUI;
  end;

var
  frmCategoryCad: TfrmCategoryCad;

implementation

uses Main, BarManFunctions;

{$R *.dfm}

{-----------------------------------------------------------------------------
  Procedure: TfrmCategoryCad.OpenGUI
  Author:    Alemao
  Date:      10/8/2004
  Result:    Fun��o que abre a tela.
-----------------------------------------------------------------------------}
procedure TfrmCategoryCad.OpenGUI;
begin
  if Assigned(frmCategoryCad) then
    frmCategoryCad.BringToFront
  else
  begin
    frmCategoryCad:= TfrmCategoryCad.Create(self);
    frmCategoryCad.Show;
  end;
end;

//----------------------------------------------------------------------------
// Fim das fun��es de usu�rio
//----------------------------------------------------------------------------

procedure TfrmCategoryCad.qrycategoryBeforePost(DataSet: TDataSet);
begin
  if qryCategory.State = dsInsert then
    qryCategory.FieldByName('categorycod').AsInteger:= GetNextKey('category', 'categorycod', frmMain.conBarMan);
end;

procedure TfrmCategoryCad.FormShow(Sender: TObject);
begin
  qryCategory.Open;
end;

procedure TfrmCategoryCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  qryCategory.Close;
  Action:= caFree;
end;

procedure TfrmCategoryCad.DBNavigatorBeforeAction(Sender: TObject; Button: TNavigateBtn);
begin
{  if Button = nbDelete then
    if MessageDlg('Deseja realmente excluir esta categoria?', mtConfirmation, [mbYes,mbNo], 0) = mrYes then
    begin
      if HasChildRegisters('contact', 'categorycod', qryCategory.FieldByName('categorycod').AsString, qryCategory.Connection) then
        MessageDlg('N�o � poss�vel excluir a categoria porque existem contatos associados a ela. '+#13#10+
                   'Para excluir, desassocie os contatos antes.', mtWarning, [mbOK], 0)
      else
        qryCategory.Delete;
    end
    else
      Button:= null;    }
end;

procedure TfrmCategoryCad.FormDestroy(Sender: TObject);
begin
  frmCategoryCad:= nil;
end;

end.
