unit ItemCad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ZAbstractRODataset, ZAbstractDataset, ZDataset, Grids,
  DBGrids, JvDBCtrl, ExtCtrls, DBCtrls;

type
  TfrmItemCad = class(TForm)
    grdContact: TJvDBGrid;
    qryItem: TZQuery;
    dtsContact: TDataSource;
    DBNavigator: TDBNavigator;
    qryItemitemcod: TSmallintField;
    qryItemcategorycod: TSmallintField;
    qryItemitemname: TStringField;
    qryItemitemvalue: TFloatField;
    qryItemitemcomission: TFloatField;
    qryItemitemobs: TMemoField;
    qryItemCategory: TZQuery;
    qryItemcategoryname: TStringField;
    procedure qryItemBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBNavigatorBeforeAction(Sender: TObject; Button: TNavigateBtn);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure OpenGUI;
  end;

var
  frmItemCad: TfrmItemCad;

implementation

uses Main, BarManFunctions;

{$R *.dfm}

{-----------------------------------------------------------------------------
  Procedure: TfrmItemCad.OpenGUI
  Author:    Alemao
  Date:      09-ago-2004
  Result:    Fun��o que abre a tela.
-----------------------------------------------------------------------------}
procedure TfrmItemCad.OpenGUI;
begin
  if Assigned(frmItemCad) then
    frmItemCad.BringToFront
  else
  begin
    frmItemCad:= TfrmItemCad.Create(self);
    frmItemCad.Show;
  end;
end;

//----------------------------------------------------------------------------
// Fim das fun��es de usu�rio
//----------------------------------------------------------------------------

procedure TfrmItemCad.qryItemBeforePost(DataSet: TDataSet);
begin
  if qryItem.State = dsInsert then
    qryItem.FieldByName('contactcod').AsInteger:= GetNextKey('contact', 'contactcod', frmMain.conBarMan);
end;

procedure TfrmItemCad.FormShow(Sender: TObject);
begin
  qryItemCategory.Open;
  qryItem.Open;
end;

procedure TfrmItemCad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qryItem.Close;
  qryItemCategory.Open;
  Action:= caFree;
end;

procedure TfrmItemCad.DBNavigatorBeforeAction(Sender: TObject; Button: TNavigateBtn);
begin
{  if Button = nbDelete then
    if MessageDlg('Deseja realmente excluir esta categoria?', mtConfirmation, [mbYes,mbNo], 0) = mrYes then
    begin
      if HasChildRegisters('contact', 'categorycod', qryItem.FieldByName('categorycod').AsString, qryItem.Connection) then
        MessageDlg('N�o � poss�vel excluir a categoria porque existem contatos associados a ela. '+#13#10+
                   'Para excluir, desassocie os contatos antes.', mtWarning, [mbOK], 0)
      else
        qryItem.Delete;
    end
    else
      Button:= null;    }
end;

procedure TfrmItemCad.FormDestroy(Sender: TObject);
begin
  frmItemCad:= Nil;
end;

end.
