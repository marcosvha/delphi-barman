object frmItemCad: TfrmItemCad
  Left = 196
  Top = 185
  Width = 790
  Height = 550
  Caption = 'Cadastro de '#237'tens'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    782
    516)
  PixelsPerInch = 96
  TextHeight = 13
  object grdContact: TJvDBGrid
    Left = 0
    Top = 0
    Width = 782
    Height = 473
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsNone
    Color = 15987699
    DataSource = dtsContact
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    ParentFont = False
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    MultiSelect = True
    TitleButtons = True
    Columns = <
      item
        Color = clSilver
        Expanded = False
        FieldName = 'itemcod'
        ReadOnly = True
        Title.Caption = 'C'#243'digo'
        Width = 43
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'categoryname'
        Title.Caption = 'Categoria'
        Width = 159
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'itemname'
        Title.Caption = 'Nome/Descri'#231#227'o'
        Width = 215
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'itemvalue'
        Title.Caption = 'Valor unit.'
        Width = 66
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'itemcomission'
        Title.Caption = 'Comiss'#227'o gar'#231'om'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'itemobs'
        Title.Caption = 'Observa'#231#227'o'
        Visible = True
      end>
  end
  object DBNavigator: TDBNavigator
    Left = 8
    Top = 482
    Width = 228
    Height = 25
    DataSource = dtsContact
    VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel, nbRefresh]
    Anchors = [akLeft, akBottom]
    Hints.Strings = (
      'First record'
      'Prior record'
      'Next record'
      'Last record'
      'Incluir'
      'Excluir'
      'Alterar'
      'Gravar altera'#231#245'es'
      'Cancelar altera'#231#245'es'
      'Atualizar')
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    BeforeAction = DBNavigatorBeforeAction
  end
  object qryItem: TZQuery
    Connection = frmMain.conBarMan
    RequestLive = True
    CachedUpdates = False
    BeforePost = qryItemBeforePost
    SQL.Strings = (
      'select * from item'
      ''
      'order by itemname'
      ''
      '/*select i.*, ic.* from item i'
      ''
      'left outer join itemcategory ic'
      'on i.categorycod = ic.categorycod'
      ''
      'order by ic.categoryname, i.itemname'
      '*/')
    ParamCheck = True
    Params = <>
    ShowRecordTypes = [utModified, utInserted, utUnmodified]
    Top = 136
    object qryItemitemcod: TSmallintField
      FieldName = 'itemcod'
      Required = True
    end
    object qryItemcategorycod: TSmallintField
      FieldName = 'categorycod'
    end
    object qryItemitemname: TStringField
      FieldName = 'itemname'
      Required = True
      Size = 100
    end
    object qryItemitemvalue: TFloatField
      FieldName = 'itemvalue'
      currency = True
    end
    object qryItemitemcomission: TFloatField
      FieldName = 'itemcomission'
      DisplayFormat = '#%'
    end
    object qryItemitemobs: TMemoField
      FieldName = 'itemobs'
      BlobType = ftMemo
    end
    object qryItemcategoryname: TStringField
      FieldKind = fkLookup
      FieldName = 'categoryname'
      LookupDataSet = qryItemCategory
      LookupKeyFields = 'categorycod'
      LookupResultField = 'categoryname'
      KeyFields = 'categorycod'
      Size = 100
      Lookup = True
    end
  end
  object dtsContact: TDataSource
    DataSet = qryItem
    Left = 32
    Top = 136
  end
  object qryItemCategory: TZQuery
    Connection = frmMain.conBarMan
    RequestLive = True
    CachedUpdates = False
    BeforePost = qryItemBeforePost
    SQL.Strings = (
      'select * from itemcategory'
      ''
      'order by categoryname'
      ''
      '/*select i.*, ic.* from item i'
      ''
      'left outer join itemcategory ic'
      'on i.categorycod = ic.categorycod'
      ''
      'order by ic.categoryname, i.itemname'
      '*/')
    ParamCheck = True
    Params = <>
    ShowRecordTypes = [utModified, utInserted, utUnmodified]
    Top = 168
  end
end
