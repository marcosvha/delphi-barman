program BarMan;

uses
  Forms,
  Main in 'Main.pas' {frmMain},
  InvoiceMan in 'InvoiceMan.pas' {frmInvoiceMan},
  CategoryCad in 'CategoryCad.pas' {frmCategoryCad},
  BarManFunctions in 'BarManFunctions.pas',
  ContactCad in 'ContactCad.pas' {frmContactCad},
  UserCad in 'UserCad.pas' {frmUserCad},
  InvoiceCad in 'InvoiceCad.pas' {frmInvoiceCad},
  ItemCad in 'ItemCad.pas' {frmItemCad},
  ItemCategoryCad in 'ItemCategoryCad.pas' {frmItemCategoryCad};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Bar Manager (C) B4IT';
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
