unit ItemCategoryCad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ZAbstractRODataset, ZAbstractDataset, ZDataset, Grids,
  DBGrids, JvDBCtrl, ExtCtrls, DBCtrls, ComCtrls, JvDBTreeView;

type
  TfrmItemCategoryCad = class(TForm)
    qryItemCategory: TZQuery;
    dtsItemCategory: TDataSource;
    DBNavigator: TDBNavigator;
    qryItemCategorycategorycod: TSmallintField;
    qryItemCategorycategoryparent: TSmallintField;
    qryItemCategorycategoryname: TStringField;
    trvItemCategory: TJvDBTreeView;
    procedure qryItemCategoryBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBNavigatorBeforeAction(Sender: TObject; Button: TNavigateBtn);
    procedure FormDestroy(Sender: TObject);
    procedure qryItemCategoryAfterDelete(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure OpenGUI;
  end;

var
  frmItemCategoryCad: TfrmItemCategoryCad;

implementation

uses Main, BarManFunctions;

{$R *.dfm}

{-----------------------------------------------------------------------------
  Procedure: TfrmItemCategoryCad.OpenGUI
  Author:    Alemao
  Date:      09-ago-2004
  Arguments: InvoiceCod: integer
  Result:    Fun��o que abre a tela.
-----------------------------------------------------------------------------}
procedure TfrmItemCategoryCad.OpenGUI;
begin
  if Assigned(frmItemCategoryCad) then
    frmItemCategoryCad.BringToFront
  else
  begin
    frmItemCategoryCad:= TfrmItemCategoryCad.Create(self);
    frmItemCategoryCad.Show;
  end;
end;

//----------------------------------------------------------------------------
// Fim das fun��es de usu�rio
//----------------------------------------------------------------------------

procedure TfrmItemCategoryCad.qryItemCategoryBeforePost(DataSet: TDataSet);
begin
  if qryItemCategory.State = dsInsert then
    qryItemCategory.FieldByName('Categorycod').AsInteger:= GetNextKey('ItemCategory', 'categorycod', frmMain.conBarMan);
end;

procedure TfrmItemCategoryCad.FormShow(Sender: TObject);
begin
  qryItemCategory.Open;
  trvItemCategory.FullExpand;
end;

procedure TfrmItemCategoryCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  qryItemCategory.Close;
  Action:= caFree;
end;

procedure TfrmItemCategoryCad.DBNavigatorBeforeAction(Sender: TObject; Button: TNavigateBtn);
begin
{  if Button = nbDelete then
    if MessageDlg('Deseja realmente excluir esta categoria?', mtConfirmation, [mbYes,mbNo], 0) = mrYes then
    begin
      if HasChildRegisters('ItemCategory', 'categorycod', qryItemCategory.FieldByName('categorycod').AsString, qryItemCategory.Connection) then
        MessageDlg('N�o � poss�vel excluir a categoria porque existem contatos associados a ela. '+#13#10+
                   'Para excluir, desassocie os contatos antes.', mtWarning, [mbOK], 0)
      else
        qryItemCategory.Delete;
    end
    else
      Button:= null;    }
end;

procedure TfrmItemCategoryCad.FormDestroy(Sender: TObject);
begin
  frmItemCategoryCad:= nil;
end;

procedure TfrmItemCategoryCad.qryItemCategoryAfterDelete(DataSet: TDataSet);
begin
  DataSet.Refresh;
  trvItemCategory.FullExpand;
end;

end.
