object frmContactCad: TfrmContactCad
  Left = 196
  Top = 185
  Width = 790
  Height = 550
  Caption = 'Cadastro de contatos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    782
    516)
  PixelsPerInch = 96
  TextHeight = 13
  object grdContact: TJvDBGrid
    Left = 0
    Top = 0
    Width = 782
    Height = 473
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsNone
    Color = 15987699
    DataSource = dtsContact
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    ParentFont = False
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    MultiSelect = True
    TitleButtons = True
    Columns = <
      item
        Color = clSilver
        Expanded = False
        FieldName = 'contactcod'
        ReadOnly = True
        Title.Caption = 'C'#243'digo'
        Width = 44
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'contactid'
        Title.Caption = 'ID (Letra)'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'contactdtcad'
        Title.Caption = 'Data cadastro'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'contactname'
        Title.Caption = 'Nome'
        Width = 191
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'contactemail'
        Title.Caption = 'E-mail'
        Width = 140
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'contactphone'
        Title.Caption = 'Telefone'
        Width = 77
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'contactcel'
        Title.Caption = 'Celular'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'contactdocument'
        Title.Caption = 'CPF/RG/CGC'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'contactbarman'
        PickList.Strings = (
          'Y'
          'N')
        Title.Caption = 'Barman?'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'contactbirthday'
        Title.Caption = 'Data nascimento'
        Width = 87
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'contactobs'
        Title.Caption = 'Observa'#231#227'o'
        Width = 146
        Visible = True
      end>
  end
  object DBNavigator: TDBNavigator
    Left = 8
    Top = 482
    Width = 228
    Height = 25
    DataSource = dtsContact
    VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel, nbRefresh]
    Anchors = [akLeft, akBottom]
    Hints.Strings = (
      'First record'
      'Prior record'
      'Next record'
      'Last record'
      'Incluir'
      'Excluir'
      'Alterar'
      'Gravar altera'#231#245'es'
      'Cancelar altera'#231#245'es'
      'Atualizar')
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    BeforeAction = DBNavigatorBeforeAction
  end
  object qryContact: TZQuery
    Connection = frmMain.conBarMan
    RequestLive = True
    CachedUpdates = False
    BeforePost = qryContactBeforePost
    SQL.Strings = (
      'select * from contact'
      'order by contactname')
    ParamCheck = True
    Params = <>
    ShowRecordTypes = [utModified, utInserted, utUnmodified]
    Top = 136
  end
  object dtsContact: TDataSource
    DataSet = qryContact
    Left = 32
    Top = 136
  end
end
