unit InvoiceCad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ZAbstractRODataset, ZAbstractDataset, ZDataset, Grids,
  DBGrids, JvDBCtrl, ExtCtrls, DBCtrls, ComCtrls, ToolWin, JvToolBar,
  StdCtrls, Buttons;

type
  TfrmInvoiceCad = class(TForm)
    grdContact: TJvDBGrid;
    qryInvoice: TZQuery;
    dtsContact: TDataSource;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    procedure qryInvoiceBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure OpenGUI;
  end;

var
  frmInvoiceCad: TfrmInvoiceCad;

implementation

uses Main, BarManFunctions, InvoiceMan;

{$R *.dfm}

{-----------------------------------------------------------------------------
  Procedure: TfrmInvoiceCad.OpenGUI
  Author:    Alemao
  Date:      09-ago-2004
  Arguments: InvoiceCod: integer
  Result:    Fun��o que abre a tela.
-----------------------------------------------------------------------------}
procedure TfrmInvoiceCad.OpenGUI;
begin
  if Assigned(frmInvoiceCad) then
    frmInvoiceCad.BringToFront
  else
  begin
    frmInvoiceCad:= TfrmInvoiceCad.Create(self);
    frmInvoiceCad.Show;
  end;  
end;

//----------------------------------------------------------------------------
// Fim das fun��es de usu�rio
//----------------------------------------------------------------------------

procedure TfrmInvoiceCad.qryInvoiceBeforePost(DataSet: TDataSet);
begin
  if qryInvoice.State = dsInsert then
    qryInvoice.FieldByName('invoicecod').AsInteger:= GetNextKey('invoice', 'invoicecod', frmMain.conBarMan);
end;

procedure TfrmInvoiceCad.FormShow(Sender: TObject);
begin
  qryInvoice.Open;
end;

procedure TfrmInvoiceCad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  try
    qryInvoice.Close;
  finally
    Action:= caFree;
  end;
end;

procedure TfrmInvoiceCad.FormDestroy(Sender: TObject);
begin
  frmInvoiceCad:= nil;
end;

procedure TfrmInvoiceCad.BitBtn1Click(Sender: TObject);
begin
  frmInvoiceMan.OpenGUI(-1);
end;

procedure TfrmInvoiceCad.BitBtn3Click(Sender: TObject);
begin
  frmInvoiceMan.OpenGUI(qryInvoice.FieldByName('invoicecod').AsInteger);
end;

procedure TfrmInvoiceCad.BitBtn2Click(Sender: TObject);
begin
  if MessageDlg('Deseja realmente excluir esta comanda?'+#13#10+
                '(Conseq�entemente todos os seus �tens ser�o exclu�dos)', mtConfirmation, [mbYes,mbNo], 0) = mrYes then
  begin
    ExecQuery('delete from invoiceitem where invoicecod = ' + qryInvoice.FieldByName('invoicecod').AsString, qryInvoice.Connection);
    ExecQuery('delete from invoice where invoicecod = '     + qryInvoice.FieldByName('invoicecod').AsString, qryInvoice.Connection);
    qryInvoice.Refresh;
  end;
end;

end.
