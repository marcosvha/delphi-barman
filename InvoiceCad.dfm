object frmInvoiceCad: TfrmInvoiceCad
  Left = 268
  Top = 172
  Width = 790
  Height = 550
  Caption = 'Cadastro de comandas'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    782
    516)
  PixelsPerInch = 96
  TextHeight = 13
  object grdContact: TJvDBGrid
    Left = 0
    Top = 0
    Width = 782
    Height = 473
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsNone
    Color = 15987699
    DataSource = dtsContact
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    ParentFont = False
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    MultiSelect = True
    TitleButtons = True
    Columns = <
      item
        Color = clSilver
        Expanded = False
        FieldName = 'invoicecod'
        ReadOnly = True
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Invoicedtcad'
        Title.Caption = 'Data/hora'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Contactcod'
        Title.Caption = 'C'#243'd. contato'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Usercod'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Invoiceclientname'
        Title.Caption = 'Cliente'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Invoiceobs'
        Title.Caption = 'Observa'#231#227'o'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'total'
        Title.Caption = 'Total'
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 8
    Top = 481
    Width = 241
    Height = 25
    Anchors = [akLeft, akBottom]
    BevelOuter = bvNone
    TabOrder = 1
    object BitBtn1: TBitBtn
      Left = 0
      Top = 0
      Width = 40
      Height = 25
      Hint = 'Incluir'
      Caption = '+'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BitBtn1Click
    end
    object BitBtn2: TBitBtn
      Left = 40
      Top = 0
      Width = 40
      Height = 25
      Hint = 'Excluir'
      Caption = '-'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = BitBtn2Click
    end
    object BitBtn3: TBitBtn
      Left = 80
      Top = 0
      Width = 40
      Height = 25
      Hint = 'Alterar'
      Caption = '^'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = BitBtn3Click
    end
    object BitBtn4: TBitBtn
      Left = 120
      Top = 0
      Width = 40
      Height = 25
      Hint = 'Atualizar'
      Caption = '<-'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
    end
  end
  object qryInvoice: TZQuery
    Connection = frmMain.conBarMan
    RequestLive = True
    CachedUpdates = False
    BeforePost = qryInvoiceBeforePost
    SQL.Strings = (
      
        'select i.invoicecod, i.Invoicedtcad, i.Contactcod, i.Usercod, i.' +
        'Invoiceclientname, i.Invoiceobs,'
      'sum(ii.invoiceitemcount * ii.invoiceitemvalue) as total '
      'from invoice i'
      'left outer join invoiceitem ii'
      'on i.invoicecod = ii.invoicecod'
      
        'group by i.invoicecod, i.Invoicedtcad, i.Contactcod, i.Usercod, ' +
        'i.Invoiceclientname, i.Invoiceobs'
      'order by i.invoicecod desc')
    ParamCheck = True
    Params = <>
    ShowRecordTypes = [utModified, utInserted, utUnmodified]
    Top = 136
  end
  object dtsContact: TDataSource
    DataSet = qryInvoice
    Left = 32
    Top = 136
  end
end
