unit ContactCad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ZAbstractRODataset, ZAbstractDataset, ZDataset, Grids,
  DBGrids, JvDBCtrl, ExtCtrls, DBCtrls;

type
  TfrmContactCad = class(TForm)
    grdContact: TJvDBGrid;
    qryContact: TZQuery;
    dtsContact: TDataSource;
    DBNavigator: TDBNavigator;
    procedure qryContactBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBNavigatorBeforeAction(Sender: TObject; Button: TNavigateBtn);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure OpenGUI;
  end;

var
  frmContactCad: TfrmContactCad;

implementation

uses Main, BarManFunctions;

{$R *.dfm}

{-----------------------------------------------------------------------------
  Procedure: TfrmContactCad.OpenGUI
  Author:    Alemao
  Date:      10/8/2004
  Result:    Fun��o que abre a tela.
-----------------------------------------------------------------------------}
procedure TfrmContactCad.OpenGUI;
begin
  if Assigned(frmContactCad) then
    frmContactCad.BringToFront
  else
  begin
    frmContactCad:= TfrmContactCad.Create(self);
    frmContactCad.Show;
  end;
end;

//----------------------------------------------------------------------------
// Fim das fun��es de usu�rio
//----------------------------------------------------------------------------

procedure TfrmContactCad.qryContactBeforePost(DataSet: TDataSet);
begin
  if qryContact.State = dsInsert then
    qryContact.FieldByName('contactcod').AsInteger:= GetNextKey('contact', 'contactcod', frmMain.conBarMan);
end;

procedure TfrmContactCad.FormShow(Sender: TObject);
begin
  qryContact.Open;
end;

procedure TfrmContactCad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  qryContact.Close;
  Action:= caFree;
end;

procedure TfrmContactCad.DBNavigatorBeforeAction(Sender: TObject; Button: TNavigateBtn);
begin
{  if Button = nbDelete then
    if MessageDlg('Deseja realmente excluir esta categoria?', mtConfirmation, [mbYes,mbNo], 0) = mrYes then
    begin
      if HasChildRegisters('contact', 'categorycod', qryContact.FieldByName('categorycod').AsString, qryContact.Connection) then
        MessageDlg('N�o � poss�vel excluir a categoria porque existem contatos associados a ela. '+#13#10+
                   'Para excluir, desassocie os contatos antes.', mtWarning, [mbOK], 0)
      else
        qryContact.Delete;
    end
    else
      Button:= null;    }
end;

procedure TfrmContactCad.FormDestroy(Sender: TObject);
begin
  frmContactCad:= nil;
end;

end.
