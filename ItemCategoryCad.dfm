object frmItemCategoryCad: TfrmItemCategoryCad
  Left = 325
  Top = 168
  Width = 438
  Height = 550
  Caption = 'Cadastro de categorias de '#237'tens'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    430
    516)
  PixelsPerInch = 96
  TextHeight = 13
  object DBNavigator: TDBNavigator
    Left = 8
    Top = 482
    Width = 112
    Height = 25
    DataSource = dtsItemCategory
    VisibleButtons = [nbDelete, nbPost, nbCancel, nbRefresh]
    Hints.Strings = (
      'First record'
      'Prior record'
      'Next record'
      'Last record'
      'Incluir'
      'Excluir'
      'Alterar'
      'Gravar altera'#231#245'es'
      'Cancelar altera'#231#245'es'
      'Atualizar')
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    BeforeAction = DBNavigatorBeforeAction
  end
  object trvItemCategory: TJvDBTreeView
    Left = 8
    Top = 8
    Width = 409
    Height = 465
    DataSource = dtsItemCategory
    MasterField = 'categorycod'
    DetailField = 'categoryparent'
    ItemField = 'categoryname'
    UseFilter = False
    PersistentNode = True
    Indent = 19
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    SortType = stText
    TabOrder = 1
    ParentFont = False
    Anchors = [akLeft, akTop, akRight, akBottom]
    HotTrack = True
    RowSelect = True
    Mirror = False
  end
  object qryItemCategory: TZQuery
    Connection = frmMain.conBarMan
    RequestLive = True
    CachedUpdates = False
    BeforePost = qryItemCategoryBeforePost
    AfterDelete = qryItemCategoryAfterDelete
    Active = True
    SQL.Strings = (
      'select * from itemcategory'
      ''
      'order by categoryname')
    ParamCheck = True
    Params = <>
    ShowRecordTypes = [utModified, utInserted, utUnmodified]
    Top = 136
    object qryItemCategorycategorycod: TSmallintField
      FieldName = 'categorycod'
      Required = True
    end
    object qryItemCategorycategoryparent: TSmallintField
      FieldName = 'categoryparent'
    end
    object qryItemCategorycategoryname: TStringField
      FieldName = 'categoryname'
      Required = True
      Size = 50
    end
  end
  object dtsItemCategory: TDataSource
    DataSet = qryItemCategory
    Left = 32
    Top = 136
  end
end
