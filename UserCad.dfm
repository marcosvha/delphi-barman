object frmUserCad: TfrmUserCad
  Left = 316
  Top = 258
  Width = 790
  Height = 550
  Caption = 'Cadastro de usu'#225'rios do sitema'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  DesignSize = (
    782
    516)
  PixelsPerInch = 96
  TextHeight = 13
  object grdUser: TJvDBGrid
    Left = 0
    Top = 0
    Width = 782
    Height = 473
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsNone
    Color = 15987699
    DataSource = dtsUser
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    ParentFont = False
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    MultiSelect = True
    TitleButtons = True
    Columns = <
      item
        Color = clSilver
        Expanded = False
        FieldName = 'usercod'
        ReadOnly = True
        Title.Caption = 'C'#243'd. usu'#225'rio'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'contactcod'
        Title.Caption = 'C'#243'digo contato'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'username'
        Title.Caption = 'Nome'
        Width = 203
        Visible = True
      end
      item
        ButtonStyle = cbsEllipsis
        Expanded = False
        FieldName = 'useraccess'
        Title.Caption = 'Permiss'#245'es'
        Width = 100
        Visible = True
      end>
  end
  object DBNavigator: TDBNavigator
    Left = 8
    Top = 482
    Width = 228
    Height = 25
    DataSource = dtsUser
    VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel, nbRefresh]
    Anchors = [akLeft, akBottom]
    Hints.Strings = (
      'First record'
      'Prior record'
      'Next record'
      'Last record'
      'Incluir'
      'Excluir'
      'Alterar'
      'Gravar altera'#231#245'es'
      'Cancelar altera'#231#245'es'
      'Atualizar')
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    BeforeAction = DBNavigatorBeforeAction
  end
  object qryUser: TZQuery
    Connection = frmMain.conBarMan
    RequestLive = True
    UpdateObject = upsUser
    CachedUpdates = False
    BeforePost = qryUserBeforePost
    SQL.Strings = (
      'select u.*, c.contactname as username'
      ' from user u, contact c'
      'where u.contactcod = c.contactcod'
      'order by contactname')
    ParamCheck = True
    Params = <>
    ShowRecordTypes = [utModified, utInserted, utUnmodified]
    Top = 136
    object qryUserusercod: TSmallintField
      FieldName = 'usercod'
      Required = True
    end
    object qryUsercontactcod: TSmallintField
      FieldName = 'contactcod'
      Required = True
    end
    object qryUseruserpass: TStringField
      FieldName = 'userpass'
      Size = 50
    end
    object qryUseruseraccess: TStringField
      FieldName = 'useraccess'
      Size = 100
    end
    object qryUserusername: TStringField
      FieldKind = fkLookup
      FieldName = 'username'
      LookupDataSet = qryContact
      LookupKeyFields = 'contactcod'
      LookupResultField = 'contactname'
      KeyFields = 'contactcod'
      Size = 100
      Lookup = True
    end
  end
  object dtsUser: TDataSource
    DataSet = qryUser
    Left = 32
    Top = 136
  end
  object qryContact: TZQuery
    Connection = frmMain.conBarMan
    RequestLive = True
    CachedUpdates = False
    BeforePost = qryUserBeforePost
    SQL.Strings = (
      'select *'
      'from contact c'
      'order by contactname')
    ParamCheck = True
    Params = <>
    ShowRecordTypes = [utModified, utInserted, utUnmodified]
    Top = 168
  end
  object upsUser: TZUpdateSQL
    DeleteSQL.Strings = (
      'delete from hd_user where usercod = :usercod')
    InsertSQL.Strings = (
      'insert into user (usercod, contactcod, userpass, useraccess)'
      'values (:usercod, :contactcod, :userpass, :useraccess)')
    ModifySQL.Strings = (
      'update user set'
      'usercod = :usercod,'
      'contactcod = :contactcod,'
      'userpass = :userpass,'
      'useraccess = :useraccess')
    Left = 64
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'usercod'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'contactcod'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'userpass'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'useraccess'
        ParamType = ptUnknown
      end>
  end
end
