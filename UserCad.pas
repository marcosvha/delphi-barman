unit UserCad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ZAbstractRODataset, ZAbstractDataset, ZDataset, Grids,
  DBGrids, JvDBCtrl, ExtCtrls, DBCtrls, ZSqlUpdate;

type
  TfrmUserCad = class(TForm)
    grdUser: TJvDBGrid;
    qryUser: TZQuery;
    dtsUser: TDataSource;
    DBNavigator: TDBNavigator;
    qryUserusercod: TSmallintField;
    qryUsercontactcod: TSmallintField;
    qryUseruserpass: TStringField;
    qryUseruseraccess: TStringField;
    qryContact: TZQuery;
    qryUserusername: TStringField;
    upsUser: TZUpdateSQL;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBNavigatorBeforeAction(Sender: TObject; Button: TNavigateBtn);
    procedure qryUserBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmUserCad: TfrmUserCad;

implementation

uses Main, BarManFunctions;

{$R *.dfm}


procedure TfrmUserCad.FormShow(Sender: TObject);
begin
  qryContact.Open;
  qryUser.Open;
end;

procedure TfrmUserCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  qryUser.Close;
  qryContact.Close;
end;

procedure TfrmUserCad.DBNavigatorBeforeAction(Sender: TObject; Button: TNavigateBtn);
begin
{  if Button = nbDelete then
    if MessageDlg('Deseja realmente excluir esta categoria?', mtConfirmation, [mbYes,mbNo], 0) = mrYes then
    begin
      if HasChildRegisters('User', 'categorycod', qryUser.FieldByName('categorycod').AsString, qryUser.Connection) then
        MessageDlg('N�o � poss�vel excluir a categoria porque existem contatos associados a ela. '+#13#10+
                   'Para excluir, desassocie os contatos antes.', mtWarning, [mbOK], 0)
      else
        qryUser.Delete;
    end
    else
      Button:= null;    }
end;

procedure TfrmUserCad.qryUserBeforePost(DataSet: TDataSet);
begin
  if qryUser.State = dsInsert then
    qryUser.FieldByName('Usercod').AsInteger:= GetNextKey('User', 'Usercod', frmMain.conBarMan);
end;

end.
