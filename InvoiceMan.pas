unit InvoiceMan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, JvMemDS, Grids, DBGrids, JvDBCtrl, ZAbstractRODataset,
  ZAbstractDataset, ZDataset, JvComponent, JvTransBtn, StdCtrls, Mask,
  JvToolEdit, JvCurrEdit, JvEdit, JvFloatEdit, ExtCtrls, jpeg;

type
  TfrmInvoiceMan = class(TForm)
    qryInvoiceItem: TZQuery;
    qryInvoice: TZQuery;
    grdInvoiceItems: TJvDBGrid;
    dtsInvoiceItem: TDataSource;
    memInvoiceItem: TJvMemoryData;
    memInvoiceItemcontactcod: TIntegerField;
    memInvoiceItemcontactid: TStringField;
    memInvoiceItemitemcod: TIntegerField;
    memInvoiceItemitemcount: TFloatField;
    qryBarman: TZQuery;
    memInvoiceItemcontactname: TStringField;
    qryItem: TZQuery;
    memInvoiceItemitemname: TStringField;
    memInvoiceIteminvoiceitemvalue: TCurrencyField;
    memInvoiceIteminvoiceitemtotal: TCurrencyField;
    pnlBottom: TPanel;
    btnFinish: TJvTransparentButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    lblInvoiceCod: TLabel;
    btnDeleteItem: TJvTransparentButton;
    btnClear: TJvTransparentButton;
    edtTotal: TJvxCurrencyEdit;
    edtIncome: TJvxCurrencyEdit;
    edtChange: TJvxCurrencyEdit;
    edtInvoiceCod: TJvFloatEdit;
    btnReset: TJvTransparentButton;
    btnClose: TJvTransparentButton;
    edtCalc: TJvCalcEdit;
    Label8: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    memCalc: TMemo;
    btnClearCalc: TJvTransparentButton;
    Image1: TImage;
    Label7: TLabel;
    Label10: TLabel;
    edtComission: TJvxCurrencyEdit;
    memInvoiceItemcomission: TCurrencyField;
    procedure grdInvoiceItemsColExit(Sender: TObject);
    procedure grdInvoiceItemsKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure memInvoiceItemCalcFields(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnFinishClick(Sender: TObject);
    procedure edtIncomeChange(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure btnDeleteItemClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure edtCalcKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnClearCalcClick(Sender: TObject);
    procedure edtCalcKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormDestroy(Sender: TObject);
    procedure memInvoiceItemitemcodChange(Sender: TField);
    procedure memInvoiceItemitemcountChange(Sender: TField);
    procedure dtsInvoiceItemDataChange(Sender: TObject; Field: TField);
  private
    iInvoiceCod: integer;
    procedure AtribuiBarman(ContactID: string);
    procedure AtribuiValorItem(ItemCod: integer);
    procedure InicializaComanda;
    procedure CarregaItemsComanda(InvoiceCod: integer);
    procedure TotalizaItens(Total, Commissao: real);
    procedure CalculaComissaoItem(ItemCod: integer);
    { Private declarations }
  public
    { Public declarations }
    procedure OpenGUI(InvoiceCod: integer);
  end;

var
  frmInvoiceMan: TfrmInvoiceMan;

implementation

uses Main, BarManFunctions;

{$R *.dfm}

{-----------------------------------------------------------------------------
  Procedure: TfrmInvoiceMan.OpenGUI
  Author:    Alemao
  Date:      09-ago-2004
  Arguments: InvoiceCod: integer
  Result:    Fun��o que abre a tela. Se passar um par�metro > 0 carrega os dados para edi��o
-----------------------------------------------------------------------------}
procedure TfrmInvoiceMan.OpenGUI(InvoiceCod: integer);
begin
  if Assigned(frmInvoiceMan) then
    frmInvoiceMan.BringToFront
  else
  begin
    frmInvoiceMan:= TfrmInvoiceMan.Create(self);
    frmInvoiceMan.iInvoiceCod:= InvoiceCod;
    frmInvoiceMan.Show;
  end;
end;

procedure TfrmInvoiceMan.AtribuiBarman(ContactID: string);
begin
  if qryBarman.Locate('contactid', ContactID, [loCaseInsensitive]) then
  begin
    memInvoiceItem.FieldByName('contactcod').AsInteger:= qryBarman.FieldByName('contactcod').AsInteger;
    memInvoiceItem.FieldByName('contactid').AsString:= qryBarman.FieldByName('contactid').AsString;
  end;
end;

procedure TfrmInvoiceMan.AtribuiValorItem(ItemCod: integer);
begin
  if qryItem.Locate('itemcod', ItemCod, []) then
    memInvoiceItem.FieldByName('invoiceitemvalue').AsFloat:= qryItem.FieldByName('itemvalue').AsFloat;
end;

procedure TfrmInvoiceMan.CalculaComissaoItem(ItemCod: integer);
begin
  if qryItem.Locate('itemcod', ItemCod, []) then
    memInvoiceItem.FieldByName('comission').AsCurrency:= (memInvoiceItem.FieldByName('invoiceitemtotal').AsFloat *
                                                          qryItem.FieldByName('itemcomission').AsFloat) / 100;
end;

procedure TfrmInvoiceMan.InicializaComanda;
begin
  try
    qryItem.Open;
    qryBarman.Open;
    memInvoiceItem.Open;

    edtTotal.Value        := 0;
    edtIncome.Value       := 0;
    edtChange.Value       := 0;
    edtInvoiceCod.Value   := 0;
    edtComission.Value    := 0;

    edtTotal.Enabled      := False;
    edtIncome.Enabled     := False;
    edtChange.Enabled     := False;
    lblInvoiceCod.Enabled := False;
    edtInvoiceCod.Enabled := False;
    edtComission.Enabled  := False;
    btnClearCalcClick(self);
  finally
  end;
end;

procedure TfrmInvoiceMan.CarregaItemsComanda(InvoiceCod: integer);
var
  rTotal, rComm: Real;
begin
  rTotal:= 0;
  rComm := 0;
  try
    dtsInvoiceItem.OnDataChange:= nil;
    with memInvoiceItem do
    begin
      Close;
      Open;

//      if not qryInvoice.Active then
//      begin
//        qryInvoice.ParamByName('iInvoiceCod').AsInteger:= InvoiceCod;
//        qryInvoice.Open;
//        Insert;
//        qryInvoice.FieldByName('invoicecod').AsInteger    := iInvoiceCod;
//        qryInvoice.FieldByName('invoicedtcad').AsDateTime := now;
//        qryInvoice.FieldByName('contactcod').AsInteger    := 1;//FieldByName('contactcod').AsInteger; // Cliente
//        qryInvoice.FieldByName('usercod').AsInteger       := 1;//FieldByName('usercod').AsInteger;    // Usu�rio que lan�ou comanda
//        qryInvoice.FieldByName('invoiceclientname').AsString:= '';//FieldByName('invoiceclientname').AsString; // Nome do cliente, que pode n�o estar cadastrado
//        qryInvoice.FieldByName('invoiceobs').AsString     := '';//FieldByName('invoiceobs').AsString;
//        qryInvoice.Post;
//      end;

      qryInvoiceItem.Close;
      qryInvoiceItem.ParamByName('iInvoiceCod').AsInteger:= iInvoiceCod;
      qryInvoiceItem.Open;
      while not qryInvoiceItem.EOF do
      begin
        Insert;
        FieldByName('contactcod').AsInteger      := qryInvoiceItem.FieldByName('contactcod').AsInteger;
        FieldByName('invoiceitemcount').AsInteger:= qryInvoiceItem.FieldByName('invoiceitemcount').AsInteger;
        FieldByName('invoiceitemvalue').AsInteger:= qryInvoiceItem.FieldByName('invoiceitemvalue').AsInteger;
        FieldByName('itemcod').AsInteger         := qryInvoiceItem.FieldByName('itemcod').AsInteger; // Deixar este campo por �ltimo, se n�o n�o calcula a comiss�o
        Post;

        rTotal:= rTotal + FieldByName('invoiceitemtotal').AsFloat;
        rComm := rComm  + FieldByName('comission').AsFloat;
        qryInvoiceItem.Next;
      end;
    end; // with
    TotalizaItens(rTotal, rComm);
  finally
    dtsInvoiceItem.OnDataChange:= dtsInvoiceItemDataChange;
  end;
end;

procedure TfrmInvoiceMan.TotalizaItens(Total, Commissao: real);
begin
  with frmInvoiceMan do
  try
    edtTotal.Value      := Total;
    edtComission.Value  := Commissao;
    edtTotal.Enabled    := True;
    edtIncome.Enabled   := True;
    edtComission.Enabled:= True;
  except
  end;  
end;

//----------------------------------------------------------------------------
// Fim das fun��es de usu�rio
//----------------------------------------------------------------------------


{-----------------------------------------------------------------------------
  Procedure: TfrmInvoiceMan.grdInvoiceItemsColExit
  Author:    Alemao
  Date:      06-ago-2004
  Arguments: Sender: TObject; var Key: Word; Shift: TShiftState
  Result:    Retorna o c�digo do contato ao pressionar a letra do ID correspondente
-----------------------------------------------------------------------------}
procedure TfrmInvoiceMan.grdInvoiceItemsColExit(Sender: TObject);
begin
  if memInvoiceItem.State in [dsInsert, dsEdit] then
    if (grdInvoiceItems.SelectedField.FieldName = 'contactid') then
      AtribuiBarman(grdInvoiceItems.SelectedField.AsString)
{    else
    if (grdInvoiceItems.SelectedField.FieldName = 'itemcod') then
      AtribuiValorItem(grdInvoiceItems.SelectedField.AsInteger)}
end;

procedure TfrmInvoiceMan.grdInvoiceItemsKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (Key= #13) and
     (grdInvoiceItems.SelectedField.FieldName = 'contactid') and
     (memInvoiceItem.State in [dsInsert, dsEdit]) then
    AtribuiBarman(grdInvoiceItems.SelectedField.AsString);
end;

procedure TfrmInvoiceMan.FormShow(Sender: TObject);
begin
  InicializaComanda;
  if iInvoiceCod > 0 then
    CarregaItemsComanda(iInvoiceCod);
end;

procedure TfrmInvoiceMan.memInvoiceItemCalcFields(DataSet: TDataSet);
begin
  memInvoiceItem.FieldByName('invoiceitemtotal').AsFloat:=  memInvoiceItem.FieldByName('invoiceitemvalue').AsFloat *
                                                            memInvoiceItem.FieldByName('invoiceitemcount').AsFloat;
end;

procedure TfrmInvoiceMan.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  try
    memInvoiceItem.Close;
    qryItem.Close;
    qryBarman.Close;
  finally
    Action:= caFree;
  end;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmInvoiceMan.btnFinishClick
  Author:    Alemao
  Date:      10-ago-2004
  Arguments: Sender: TObject
  Result:    Grava todos os dados em mem�ria para o banco de dados e calcula os totais,
             liberando a tela para novas opera��es.
-----------------------------------------------------------------------------}
procedure TfrmInvoiceMan.btnFinishClick(Sender: TObject);
var
  rTotal, rComm: Real;
begin
  rTotal:= 0;
  rComm := 0;
  try
    with memInvoiceItem do
    begin
      if State in [dsInsert, dsEdit] then
        Post;
      First;

      if iInvoiceCod = -1 then
        iInvoiceCod:= GetNextKey('invoice', 'invoicecod', qryInvoice.Connection);
      if not qryInvoice.Active then
      begin
        qryInvoice.ParamByName('iInvoiceCod').AsInteger:= iInvoiceCod;
        qryInvoice.Open;
        qryInvoice.Insert;
        qryInvoice.FieldByName('invoicecod').AsInteger    := iInvoiceCod;
        qryInvoice.FieldByName('invoicedtcad').AsDateTime := now;
        qryInvoice.FieldByName('contactcod').AsInteger    := 1;//FieldByName('contactcod').AsInteger; // Cliente
        qryInvoice.FieldByName('usercod').AsInteger       := 1;//FieldByName('usercod').AsInteger;    // Usu�rio que lan�ou comanda
        qryInvoice.FieldByName('invoiceclientname').AsString:= '';//FieldByName('invoiceclientname').AsString; // Nome do cliente, que pode n�o estar cadastrado
        qryInvoice.FieldByName('invoiceobs').AsString     := '';//FieldByName('invoiceobs').AsString;
        qryInvoice.Post;
      end; // if

      while not EOF do
      begin
        if not qryInvoiceItem.Active then
        begin
          qryInvoiceItem.ParamByName('iInvoiceCod').AsInteger:= iInvoiceCod;
          qryInvoiceItem.Open;
          qryInvoiceItem.Insert;
          qryInvoiceItem.FieldByName('invoicecod').AsInteger      := iInvoiceCod;
          qryInvoiceItem.FieldByName('contactcod').AsInteger      := FieldByName('contactcod').AsInteger;
          qryInvoiceItem.FieldByName('itemcod').AsInteger         := FieldByName('itemcod').AsInteger;
          qryInvoiceItem.FieldByName('invoiceitemcount').AsInteger:= FieldByName('invoiceitemcount').AsInteger;
          qryInvoiceItem.FieldByName('invoiceitemvalue').AsInteger:= FieldByName('invoiceitemvalue').AsInteger;
          qryInvoiceItem.Post;
        end; // if

        rTotal:= rTotal + FieldByName('invoiceitemtotal').AsFloat;
        rComm := rComm  + FieldByName('comission').AsFloat;
        Next;
      end; // while
    end; // with

    TotalizaItens(rTotal, rComm);
    edtIncome.SetFocus;
  except
  end;
end;

procedure TfrmInvoiceMan.edtIncomeChange(Sender: TObject);
begin
    edtChange.Enabled := True;
    edtChange.Value   := edtIncome.Value - edtTotal.Value;
    if edtChange.Value < 0 then
      edtChange.Font.Color:= clMaroon
    else
      edtChange.Font.Color:= $004080FF;
end;

procedure TfrmInvoiceMan.btnClearClick(Sender: TObject);
begin
  memInvoiceItem.Close;
  InicializaComanda;
end;


{-----------------------------------------------------------------------------
  Procedure: TfrmInvoiceMan.btnDeleteItemClick
  Author:    Alemao
  Date:      10-ago-2004
  Arguments: Sender: TObject
  Result:    Suporta multipla sele��o, portanto, precisa varrer todos os selecionados
-----------------------------------------------------------------------------}
procedure TfrmInvoiceMan.btnDeleteItemClick(Sender: TObject);
var
  i: integer;
begin
  if not (memInvoiceItem.EOF and memInvoiceItem.BOF) then
  begin
    if grdInvoiceItems.SelCount < 2 then
      memInvoiceItem.Delete
    else
      for i:= 0 to grdInvoiceItems.SelCount-1 do
        grdInvoiceItems.SelectedRows.Delete;
    memInvoiceItem.Refresh;
  end; // if  
end;

procedure TfrmInvoiceMan.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmInvoiceMan.btnResetClick(Sender: TObject);
begin
  memInvoiceItem.Close;
  InicializaComanda;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmInvoiceMan.edtCalcKeyUp
  Author:    Alemao
  Date:      06-ago-2004
  Arguments: Sender: TObject; var Key: Word; Shift: TShiftState
  Result:    Adiciona os valores dos c�lculos para a "bobina"
-----------------------------------------------------------------------------}
procedure TfrmInvoiceMan.edtCalcKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
//  ShowMessage(inttostr(key));
  if (Key = vk_return) or
     (Key = 187) then
  begin
    memCalc.Lines.Add('----------');
    memCalc.Lines.Add(edtCalc.Text);
    memCalc.Lines.Add('');
  end;
end;

procedure TfrmInvoiceMan.btnClearCalcClick(Sender: TObject);
begin
  edtCalc.Value:= 0;
  memCalc.Lines.Clear;
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmInvoiceMan.edtCalcKeyDown
  Author:    Alemao
  Date:      06-ago-2004
  Arguments: Sender: TObject; var Key: Word; Shift: TShiftState
  Result:    Insere o �ltimo valor antes de calcular o resultado na "bobina"
-----------------------------------------------------------------------------}
procedure TfrmInvoiceMan.edtCalcKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Key = vk_return) or
     (Key = 187) then
    memCalc.Lines.Add(edtCalc.Text)
  else
  if Key = 107 then
    memCalc.Lines.Add(edtCalc.Text + '+')
  else
  if Key = 109 then
    memCalc.Lines.Add(edtCalc.Text + '-')
  else
  if Key = 106 then
    memCalc.Lines.Add(edtCalc.Text + '*')
  else
  if Key = 111 then
    memCalc.Lines.Add(edtCalc.Text + '/');

end;

{-----------------------------------------------------------------------------
  Procedure: TfrmInvoiceMan.memInvoiceItemBeforePost
  Author:    Alemao
  Date:      07-ago-2004
  Arguments: DataSet: TDataSet
  Result:    Antes de gravar, calcula a comiss�o de acordo com o percentual do item
-----------------------------------------------------------------------------}
procedure TfrmInvoiceMan.FormDestroy(Sender: TObject);
begin
  frmInvoiceMan:= nil;
end;

procedure TfrmInvoiceMan.memInvoiceItemitemcodChange(Sender: TField);
begin
  AtribuiValorItem(memInvoiceItem.FieldByName('itemcod').AsInteger);
  CalculaComissaoItem(memInvoiceItem.FieldByName('itemcod').AsInteger);
end;

procedure TfrmInvoiceMan.memInvoiceItemitemcountChange(Sender: TField);
begin
  CalculaComissaoItem(memInvoiceItem.FieldByName('itemcod').AsInteger);
end;

{-----------------------------------------------------------------------------
  Procedure: TfrmInvoiceMan.dtsInvoiceItemStateChange
  Author:    Alemao
  Date:      10-ago-2004
  Arguments: Sender: TObject
  Result:    Se ficou uma vez em edi��o, permite finalizar. Aten��o no carregamento de itens j� salvos
-----------------------------------------------------------------------------}
procedure TfrmInvoiceMan.dtsInvoiceItemDataChange(Sender: TObject;
  Field: TField);
begin
  if (not btnFinish.Enabled) and
    (memInvoiceItem.state in [dsInsert, dsEdit]) then
    btnFinish.Enabled:= True;
end;

end.
