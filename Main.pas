unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, JvComponent, JvTransBtn, ZConnection, StdCtrls, XPMan,
  ExtCtrls, jpeg, ZAbstractConnection, JvExControls, JvButton,
  JvTransparentButton;

type
  TfrmMain = class(TForm)
    btnAddInvoice: TJvTransparentButton;
    btnInvoice: TJvTransparentButton;
    btnUser: TJvTransparentButton;
    btnContact: TJvTransparentButton;
    btnCategory: TJvTransparentButton;
    btnItemCategory: TJvTransparentButton;
    btnItem: TJvTransparentButton;
    conBarMan: TZConnection;
    XPManifest: TXPManifest;
    Image1: TImage;
    procedure btnAddInvoiceClick(Sender: TObject);
    procedure btnCategoryClick(Sender: TObject);
    procedure btnContactClick(Sender: TObject);
    procedure btnUserClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnInvoiceClick(Sender: TObject);
    procedure btnItemCategoryClick(Sender: TObject);
    procedure btnItemClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses InvoiceMan, CategoryCad, ContactCad, UserCad, InvoiceCad, ItemCad,
  ItemCategoryCad;

{$R *.dfm}

procedure TfrmMain.btnAddInvoiceClick(Sender: TObject);
begin
  frmInvoiceMan.OpenGUI(-1);
end;

procedure TfrmMain.btnCategoryClick(Sender: TObject);
begin
  frmCategoryCad.OpenGUI;
end;

procedure TfrmMain.btnContactClick(Sender: TObject);
begin
  frmContactCad.OpenGUI;
end;

procedure TfrmMain.btnUserClick(Sender: TObject);
begin
  frmUserCad:= TfrmUserCad.Create(self);
  try
    frmUserCad.ShowModal;
  finally
    FreeAndNil(frmUserCad);
  end;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  if Date > StrToDate('08/02/2007') then
  begin
    MessageDlg('A validade deste programa expirou.'+#13+#10+'Entre em contato com Marcos (51) 8123-4459', mtWarning, [mbOK], 0);
    Application.Terminate;
  end;
end;

procedure TfrmMain.btnInvoiceClick(Sender: TObject);
begin
  frmInvoiceCad.OpenGUI;
end;

procedure TfrmMain.btnItemCategoryClick(Sender: TObject);
begin
  frmItemCategoryCad.OpenGUI;
end;

procedure TfrmMain.btnItemClick(Sender: TObject);
begin
  frmItemCad.OpenGUI;
end;

end.
